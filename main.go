package main

import (
	"flag"
	"io/ioutil"
	"os"
	"regexp"
)

func main() {
	in := flag.String("i", "", "File to convert")
	out := flag.String("o", "", "Output file")

	flag.Parse()

	rx := regexp.MustCompile(`(?m)(tts:color="rgba\((?:\d\d?\d?,?){3,4}\)").?([^>]*)(?:>)(.*)(?:</p>)`)
	rp := "$2><span $1>$3</span></p>"

	file, err := ioutil.ReadFile(*in)
	if err != nil {
		panic(err)
	}

	fixed := rx.ReplaceAll(file, []byte(rp))

	outFile, err := os.Create(*out)
	if err != nil {
		panic(err)
	}

	outFile.Write(fixed)

	outFile.Close()
}
